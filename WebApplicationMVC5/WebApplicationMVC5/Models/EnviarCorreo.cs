﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationMVC5.Models
{
    public class EnviarCorreo
    {
        public static void enviarCorreos(string subject)
        {
            var v_subjectgenerica = "sin asunto";

            System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();
            mmsg.To.Add("davidescotoflores@gmail.com");

            if (subject.Length <=0)
                mmsg.Subject = v_subjectgenerica;
            else 
                mmsg.Subject = subject;

            mmsg.SubjectEncoding = System.Text.Encoding.UTF8;
            mmsg.Body = "Prueba de envio de correo desde aplicaciones .NET MVC5";
            mmsg.BodyEncoding = System.Text.Encoding.UTF8;
            mmsg.IsBodyHtml = false;
            mmsg.From = new System.Net.Mail.MailAddress("creativa.oc@gmail.com");

            System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();
            cliente.Credentials = new System.Net.NetworkCredential("creativa.oc@gmail.com", "CreativaOC$");
            cliente.Port = 587;
            cliente.EnableSsl = true;
            cliente.Host = "smtp.gmail.com";

            string output = null;
            try
            {
                cliente.Send(mmsg);
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                output = "Error enviando correo electrónico: " + ex.Message;
            }
        }
    }
}